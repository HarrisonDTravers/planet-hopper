using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GravityBody : MonoBehaviour
{
    public GameObject planet;

    public float maxGravity;
    public float maxGravityDistance;

    public Rigidbody Rigidbody { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector2.Distance(planet.transform.position, this.transform.position);

        Vector3 v = planet.transform.position - transform.position;
        Rigidbody.AddForce(v.normalized * (1.0f - dist / maxGravityDistance) * maxGravity);
    }
}
