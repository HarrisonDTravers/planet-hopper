using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUG_DragCollider : MonoBehaviour
{
    public Vector3 lastMousePosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Physics.Raycast(mousePos, Vector3.forward, out RaycastHit hitInfo, 10))
            {
                hitInfo.collider.transform.position = new Vector3(mousePos.x, mousePos.y, hitInfo.collider.transform.position.z);
                hitInfo.collider.GetComponent<Rigidbody>().velocity = Vector3.zero;
                hitInfo.collider.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }

            lastMousePosition = mousePos;
        }
    }
}
